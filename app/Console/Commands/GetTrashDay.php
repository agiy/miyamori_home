<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Maknz\Slack\Facades\Slack;

class GetTrashDay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:get:trash';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '今日は何のゴミの日かSlackで通知';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //曜日を日本語で取得
        setlocale(LC_ALL, 'ja_JP.UTF-8');
        $date = Carbon::now();

        if ($date->isMonday() || $date->isThursday()) {
            $trashItem = '燃えるゴミ';
        } elseif ($date->isFriday()) {
            $trashItem = '資源ゴミ';
        } elseif (Carbon::now()->weekOfMonth == 4 && $date->isSaturday()) {
            $trashItem = '燃えないゴミ';
        }

        if ( ! empty($trashItem)) {
            Slack::to('#trash')->send('今日は'.$trashItem.'の日!');
            $this->info('完了');

            return;
        }
        $this->info('ゴミの日じゃない');
    }
}
